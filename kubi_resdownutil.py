from urllib import request
import json
import os

versions_json="https://launchermeta.mojang.com/mc/game/version_manifest.json"
req=request.Request(versions_json)
resp=request.urlopen(req)
versions_jdata=resp.read()
versions_jdata=json.loads(versions_jdata)
target_os="natives-linux"

versions={}

for version in versions_jdata["versions"]:
    versions[version["id"]]={"url":version["url"],"type":version["type"]}

n=0
ordered_versions={}
for v in versions:
    n+=1
    print(str(n)+") "+v)
    ordered_versions[str(n)]=v


print("Enter version number: ");vchoise=str(input());os.system("clear")

vchoised=ordered_versions[str(vchoise)];vchoised=versions[vchoised]

req=request.Request(vchoised["url"])
resp=request.urlopen(req)
libraries=json.loads(resp.read())


for lib in libraries["libraries"]:
    __downloads=lib["downloads"]
    if "classifiers" in __downloads:
        if "natives-linux" in __downloads["classifiers"]:
            #print(__downloads["classifiers"]["natives-linux"])
            download_target=__downloads["classifiers"]["natives-linux"]
            __path=download_target["path"][:len(download_target)-len(lib["name"])]
            print(__path)
            if not os.path.exists("minecraft/"+__path):
                os.makedirs("minecraft/"+__path)
            __file_obj=request.urlopen(request.Request(download_target["url"]))
            f=open("minecraft/"+download_target["path"],"wb")
            f.write(__file_obj)
            f.close()
            __file_obj=None