# -*- coding: utf-8 -*-
#/usr/bin/python3

import urllib.request
import json

json_data=urllib.request.urlopen("https://status.mojang.com/check").read()

json_data=json_data.decode('utf8')

newdata=(json_data[1:-1].split(","))


def statcolor(stat):
	output=str()
	if stat=="green":
		output='\033[1;32m'+"⬤"+'\033[1;m'
	if stat=="yellow":
		output='\033[1;33m'+"⬤"+'\033[1;m'
	if stat=="red":
		output='\033[1;31m'+"⬤"+'\033[1;m'
	return(output)

for stat in newdata:
	newstat=stat.replace('"',"").replace("{","").replace("}","").split(":")
	print(statcolor(newstat[1])+ "  "+newstat[0])
	
