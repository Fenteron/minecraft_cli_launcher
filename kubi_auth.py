from urllib import request
import json
import getpass

print("Enter Username: ");username=str(input())
print("Enter Password: ");password=str(getpass.getpass())

data = json.dumps({"agent":{"name":"Minecraft","version":1}, "username":username,"password":password})
data = str(data).encode("utf-8")

headers={'Content-Type': 'application/json'}

req=request.Request("https://authserver.mojang.com/authenticate",data=data,headers=headers)
resp=request.urlopen(req)
print(resp.read())